#include "glut.h"
#include "MundoServidor.h"

//el unico objeto global
CMundoServidor servidor;

//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	

int main(int argc,char* argv[])
{
	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Mundo");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	servidor.InitGL();

	
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();

	return 0;
}

void OnDraw(void)
{
	servidor.OnDraw();
}
void OnTimer(int value)
{
	servidor.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
	if (servidor.puntos1 >= 7 || servidor.puntos2 >= 7) servidor.~CMundoServidor();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	servidor.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
